from django.contrib import admin
from .models import Post

# Register your models here.
# admin.site.register(Post)
"""You can register this using this too method.
admin.site.register(Post, PostAdmin) or
@admin.register(Post) This is a decorator as in python language
"""

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'author', 'publish',
                    'status')
    list_filter = ('status', 'created', 'publish', 'author')
    search_fields = ('title', 'body')
    prepopulated_fields = {'slug':('title',)}
    raw_id_fields = ('author',)
    date_hierarchy = 'publish'
    ordering = ('status', 'publish')